**DECOUVERTE VAGRANT**

**LAB_1**

**Creation d'un repertoire :**

- Commande "mkdir lab" - puis "cd lab" pour se placer dans le repertoire  


**Liste des commandes de base :**


Vagrant init -- Créé un fichier Vagrantfile (configuration des VM)

Vagrant validate -- Vérifie la conformité du fichier Vagrantfile

Vagrant up -- Démarre ou Créé une VM en fonction des paramètres définis dans Vagrantfile

Vagrant status -- Affiche l'etat actuel de la VM 

Vagrant global-status -- Affiche l'etat de toutes les VM sur la machine hote

Vagrant ssh -- Se connecte a une VM en ssh

Vagrant halt -- Arrete la machine

Vagrant destroy -- Supprime une VM

Vagrant box add ubuntu/trusty64 -- Télécharge et ajoute une nouvelle image à l'emplacment suivant 
C:\Users\mika\.vagrant.d\boxes

Vagrant init ubuntu/trusty64 -- Créé un fichier Vagrantfile spécifique à la machine ubuntu/trusty64




**LAB_2**

**Créer un vbox**

**Création d'un nouveau repertoire :**

-- mkdir lab-2 --
-- cd lab-2 --

**Création d'une VM :** 

-- vagrant box add --box-version 20190425.0.0 ubuntu/trusty64 -- Télécharge une image en précisant la version 

-- vagrant init ubuntu/trusty64 -- Crée le fichier Vagrantfile

-- vagrant up -- Crée la VM et la Démarre dans VirtualBox

**Connection en SSH :**

-- vagrant ssh --

**Installer nginx :** 

-- sudo apt install nginx -- Installe nginx

-- sudo service nginx start -- Démarre nginx

-- sudo service nginx enable -- Active son lancement au démarrage de la VM

**Creer une vbox :** 

-- vagrant package --output vbox-mika.box -- Crée un fichiers .box (capture une image de la vm)

-- creer un compte Vagrant Cloud --

-- uploader le fichier .box & add provider -- 

https://app.vagrantup.com/mickabages/boxes/nginx/versions/1


**LAB-3**

**Création du repertoire lab-3** 

-- mkdir lab-3 -- cd lab-3 --

**Création du fichier Vagrantfile minimaliste**

-- vagrant init -m -- le -m est l'argument pour minimaliste

-- effectuer les modifications du vagrantfile pour modifier les paramètres de la VM --

**-*- mode: ruby -*-**
**vi: set ft=ruby :**

**Variabilisation des ressources**

RAM = 2048
CPU = 2

**Configuration de la VM**

Vagrant.configure("2") do |config|

  config.vm.box = "geerlingguy/centos7"

  config.vm.provider "virtualbox" do |vb|

    vb.name = "vm-mika"

	  vb.cpus = CPU
    
	  vb.memory = RAM
  end

**Installation de nginx**
	
  config.vm.provision "shell", inline: <<-SHELL

	sudo yum install -y epel-release

	sudo yum install -y nginx

	sudo systemctl start nginx

	sudo systemctl enable nginx

  SHELL
  
end

**Création de la VM**

-- vagrant up --


**LAB_4**

**Configuration d'une IP**

On crée la variable -PRIVATE_IP = "0.0.0.0"-

On appelle la variable au debut de la boucle -config-

-- config.vm.network "private_network", ip: $PRIVATE_IP --


**LAB-5**

**Configurer plusieurs VM dans le Vagrantfile**

Pour cela il faut creer un repertoire propre a chaques VM dans le fichier .vagrant et definir un nom grace a la commande :

-- config.vm.define "X" do |X| --

Definir en suivant les paramètres comme vu dans les LAB précédents.

la commande echo des VM 2 et 3 ecris le texte "Serveur WEB" directement dans le fichier index.html grace a l'utilisation de --tee-

-- echo "Serveur WEB2" | sudo tee /var/www/html/index.html --


**LAB-6**

**Configuration d'un plugin**

Le Plugin --vagrant-hostmanager-- édite automatiquement le hostname et l'ip des vm dans le fichier host de la machine hote

system("plugin install vagrant-hostmanager") -- Installe le plugin --

Vagrant.configure("2") do |config|           -- Configuration a l'execution du script -- 

  config.hostmanager.enabled = true

  config.hostmanager.manage_host = true

  config.hostmanager.manage_guest = true

  config.hostmanager.ignore_private_ip = false
  
  config.hostmanager.include_offline = true
